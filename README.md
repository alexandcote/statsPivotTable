# statsPivotTable : Use PivotTable.js for statistics in your survey. #

## Usage

After installation and activation : user with statistics permission on survey have a new item on tools menu.

This allow to use [Pivottable.js](https://pivottable.js.org) to have a lot of information on the survey.

## Installation

### Via GIT
- Go to your LimeSurvey Directory
- Clone in plugins/statsPivotTable directory : `git clone https://git.framasoft.org/SondagePro-LimeSurvey-plugin/statsPivotTable.git statsPivotTable`

### Via ZIP dowload
- Download <http://extensions.sondages.pro/IMG/auto/statsPivotTable.zip>
- Extract : `unzip statsPivotTable.zip`
- Move the directory to  plugins/ directory inside LimeSurvey

## Home page & Copyright
- HomePage <http://extensions.sondages.pro/>
- Copyright © 2017 Denis Chenu <http://sondages.pro>
- Licence : GNU Affero General Public License <https://www.gnu.org/licenses/agpl-3.0.html>
- This plugin use
    - [Pivottable.js](https://pivottable.js.org) © 2012-2013 Nicolas Kruchten, Datacratic, other contributors.
    - [D3.js](https://d3js.org/) © 2010-2016, Michael Bostock.
    - [C3.js](http://c3js.org//) © 2013 Masayuki Tanaka.
